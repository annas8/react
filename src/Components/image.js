import ImageBack from "./img/top.png";
import './image.css';

function Image () {
    return (
        <div className="container-img">
        <img 
        src={ImageBack}
        alt="background"
        />
        </div>
    );
}

export default Image;