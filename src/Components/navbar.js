import './navbar.css';

function Navbar () {
    return (
    <header>
        <div className='container'>
          <nav>
            <a href='/#'>HOME</a>
            <a a href='/#'>PHOTOAPP</a>
            <a a href='/#'>DESIGN</a>
            <a a href='/#'>DOWNLOAD</a>
          </nav>
        </div>
    </header>
    );
}

export default Navbar;