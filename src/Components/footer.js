import './footer.css';

function Footer () {
    return (
        <footer>
            <div className='container-footer'>
                <p>Copyriting by phototime - all right reserved</p>
            </div>
        </footer>
    );
}

export default Footer;