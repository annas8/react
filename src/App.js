import './App.css';
import Navbar from './Components/navbar';
import Image from './Components/image';
import Content from './Components/content';
import Footer from './Components/footer';

function App() {
  return (
    <div>
    <Navbar/>
    <Image/>
    <Content/>
    <Footer/>
    </div>
  );
}

export default App;
